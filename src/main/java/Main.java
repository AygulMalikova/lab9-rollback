import java.sql.*;
import java.util.Arrays;
import java.util.LinkedList;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) {
        add_salaries(
                new LinkedList<Integer>(Arrays.asList(100, 101)),
                new LinkedList<Integer>(Arrays.asList(107, 108)),
                new LinkedList<Integer>(Arrays.asList(51000, 67000))
        );
    }

    public static void add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            int total_rows = Math.max(ids.size(), Math.max(employee_ids.size(), amount.size()));
            String SQL;

            for (int i = 0; i < total_rows; i++) {
                System.out.println("Inserting one row....");
                SQL = "INSERT INTO Salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employee_ids.get(i) + "," +
                        amount.get(i) + ")";

                stmt.executeUpdate(SQL);
            }

            System.out.println("Commiting data here....");
            conn.commit();

            String sql = "SELECT id, employee_id, amount FROM Salary";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            print_rs_salary(rs);

            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }//end try

        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

    public static void print_rs_salary(ResultSet rs) throws SQLException {
        //Ensure we start with first row
        rs.beforeFirst();
        while (rs.next()) {
            //Retrieve by column name
            int id = rs.getInt("id");
            int employee_id = rs.getInt("employee_id");
            int amount = rs.getInt("amount");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Employee id: " + employee_id);
            System.out.println(", Amount: " + amount);
        }
        System.out.println();
    }


//    public static void add_employees() {
//        Connection conn = null;
//        Statement stmt = null;
//        try{
//            Class.forName("org.postgresql.Driver");
//            System.out.println("Connecting to database...");
//            conn = DriverManager.getConnection(DB_URL,USER,PASS);
//            conn.setAutoCommit(false);
//
//
//            System.out.println("Creating statement...");
//            stmt = conn.createStatement(
//                    ResultSet.TYPE_SCROLL_INSENSITIVE,
//                    ResultSet.CONCUR_UPDATABLE);
//
//            System.out.println("Inserting one row....");
//            String SQL = "INSERT INTO Employees " +
//                    "VALUES (108, 20, 'Rita', 'Tez')";
//
//            stmt.executeUpdate(SQL);
//            SQL = "INSERT INTO Employees " +
//                    "VALUES (107, 22, 'Sita', 'Singh')";
//            stmt.executeUpdate(SQL);
//
//            // Commit data here.
//            System.out.println("Commiting data here....");
//            conn.commit();
//
//            String sql = "SELECT id, first, last, age FROM Employees";
//            ResultSet rs = stmt.executeQuery(sql);
//            System.out.println("List result set for reference....");
//            print_rs_employee(rs);
//
//            rs.close();
//            stmt.close();
//            conn.close();
//        }catch(SQLException se){
//            //Handle errors for JDBC
//            se.printStackTrace();
//            // If there is an error then rollback the changes.
//            System.out.println("Rolling back data here....");
//            try{
//                if(conn!=null)
//                    conn.rollback();
//            }catch(SQLException se2){
//                se2.printStackTrace();
//            }//end try
//
//        }catch(Exception e){
//            //Handle errors for Class.forName
//            e.printStackTrace();
//        }finally{
//            //finally block used to close resources
//            try{
//                if(stmt!=null)
//                    stmt.close();
//            }catch(SQLException se2){
//            }
//            try{
//                if(conn!=null)
//                    conn.close();
//            }catch(SQLException se){
//                se.printStackTrace();
//            }//end finally try
//        }
//        System.out.println("Goodbye!");
//    }
//
//
//    public static void print_rs_employee(ResultSet rs) throws SQLException{
//        //Ensure we start with first row
//        rs.beforeFirst();
//        while(rs.next()){
//            //Retrieve by column name
//            int id  = rs.getInt("id");
//            int age = rs.getInt("age");
//            String first = rs.getString("first");
//            String last = rs.getString("last");
//
//            //Display values
//            System.out.print("ID: " + id);
//            System.out.print(", Age: " + age);
//            System.out.print(", First: " + first);
//            System.out.println(", Last: " + last);
//        }
//        System.out.println();
//    }

}
